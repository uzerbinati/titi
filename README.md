# README #

Is a simple server side programming language build to be an alternative to PHP.

### Variable ###

You could define a variable using the following command:

If we give text string value:
    
```
#!TITI
$A = "Hello World"
```
if we give a variable value 


```
#!TITI
$A = $B
```
### Python Command ###
You could run a python command inside in TITI script:

```
#!TITI
python ("print ' hello world ' ")
```

### Function ###
The class parameter are : @A,@B,@C ecc
The function parameter are: @0,@1,@2 ecc
You could create a function with the following code:
```
#!TITI
#class_of_the_function.name_of_function {
      python ("print 'first class parameter'")
      python ("print @A")
      python ("print 'first function parameter'")
      python ("print @0")
}
```
And run it :

```
#!TITI
#class_name.function_name ('parameter_one' ; 'parameter_two' ; )
```

### Class ###
You give the parameter to the class in the following way:

```
#!TITI
@class_name ( 'parameter_one' ; 'parameter_two';)
```